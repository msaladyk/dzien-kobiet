import React from "react";
import "./App.css";

const App = () => {
  return (
    <div className="background">
      <h1 className="header rotating">Wszystkiego najlepszego</h1>
    </div>
  );
};

export default App;
